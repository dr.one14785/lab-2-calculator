package org.fyntem.operation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationTest {

    @Test
    public void add2PositiveNumbers() {
        Operation operation = new OperationAdd();
        final String result = operation.calculateTwoNumbers(5, 7);

        assertEquals("12", result);
    }

    @Test
    public void add2NegativeNumbers() {
        Operation operation = new OperationAdd();
        final String result = operation.calculateTwoNumbers(-5, -7);

        assertEquals("-12", result);
    }

    @Test
    public void add2PositiveEnourmousNumbers() {
        Operation operation = new OperationAdd();
        final String result = operation.calculateTwoNumbers(2000000000, 1000000000);

        //TODO/FIX
        assertEquals("3000000000", result);
    }

    @Test
    public void subtract2PositiveNumbersN1BiggerThanN2() {
        Operation operation = new OperationSub();
        final String result = operation.calculateTwoNumbers(2000000000, 1000000000);

        assertEquals("1000000000", result);
    }

    @Test
    public void subtract2PositiveNumbersN1LesserThanN2() {
        Operation operation = new OperationSub();
        final String result = operation.calculateTwoNumbers(1000000000, 2000000000);

        assertEquals("1000000000", result);
    }

    @Test
    public void multiply5Numbers() {

    }

    @Test
    public void multiply2PositiveAnd1NegativeNumbers() {

    }

    @Test
    public void divide2PositiveNumbers() {

    }

    @Test
    public void divide2NegativeNumbers() {

    }

    @Test
    public void divideOnZero() {

    }
}
