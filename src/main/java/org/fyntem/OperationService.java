package org.fyntem;

import org.fyntem.operation.Operation;
import org.fyntem.operation.OperationAdd;
import org.fyntem.operation.OperationSub;

public final class OperationService {

    public static Operation getOperationInstance(String operation) {
        switch (operation) {
            case "ADD":
                return new OperationAdd();
            case "SUB":
                return new OperationSub();
            case "MULT":
                // TODO: implement
                return null;
            case "DIV":
                // TODO: implement
                return null;
            default:
                throw new IllegalArgumentException("There is no such operation: " + operation);
        }
    }
}
