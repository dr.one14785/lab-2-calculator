package org.fyntem.operation;

public interface Operation {

    String calculateTwoNumbers(int numberOne, int numberTwo);
}
