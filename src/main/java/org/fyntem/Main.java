package org.fyntem;

import org.fyntem.operation.Operation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter first number:");
        int n1 = scanner.nextInt();

        System.out.println("Enter operation (ADD, SUB, MULT, DIV):");
        String operation = scanner.next();

        System.out.println("... and now second number:");
        int n2 = scanner.nextInt();

        // Define which operation should be used based on user input, and return corresponding class for it's handling
        Operation operationInstance = OperationService.getOperationInstance(operation);
        System.out.println("Result of operation " + operation + " is: " + operationInstance.calculateTwoNumbers(n1, n2));
    }

}